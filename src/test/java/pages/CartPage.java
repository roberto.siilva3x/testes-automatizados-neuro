package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static constants.ProductDescript.FIRST_PRODUCT_DESCRIPTION;

public class CartPage extends BasePage{
    public CartPage(WebDriver driver) {
        super(driver);
    }

    public static By FIRST_PRODUCT_CART = By.xpath("//a[contains(@class, 'productName')]");

    public void productCartValidate() {
        Assert.assertEquals(FIRST_PRODUCT_DESCRIPTION,
                getTextByAttribute(FIRST_PRODUCT_CART, "innerText"));
    }


}
