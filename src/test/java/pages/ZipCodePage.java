package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ZipCodePage extends BasePage{
    public ZipCodePage(WebDriver driver) {
        super(driver);
    }

    public static By ZIP_CODE_INPUT = By.xpath("//input[@data-testid = 'ZipCodeInput']");
    public static By ZIP_CODE_BTN = By.xpath("//button[@id = 'botaoCalcularFrete']");

    public static By FIRST_FRETE_PRICE = By.xpath("//div[@id = 'listaOpcoesFrete']//div[2]//span[1]");
    public static By SECOND_FRETE_PRICE = By.xpath("//div[@id = 'listaOpcoesFrete']/div[2]/div[2]/span[1]");
    public static By CLOSE_ZIP_CODE = By.xpath("//div[@data-testid = 'btnClose']");
    public static By COMPRAR_BTN  = By.xpath("//div[@id = 'blocoValores']//button");

    public void fillZipCode(String zipcode) {
        click(ZIP_CODE_INPUT);
        send_keys(ZIP_CODE_INPUT, zipcode);
        click(ZIP_CODE_BTN);
    }

    public void getAndPrintZipCode() {
        System.out.println("Valor do frete GFL: " +getTextByAttribute(FIRST_FRETE_PRICE, "innerText"));
        System.out.println("Valor do frete sedex : " +getTextByAttribute(SECOND_FRETE_PRICE, "innerText"));
        click(CLOSE_ZIP_CODE);
    }

    public void clickOnComprarBtn() {
        click(COMPRAR_BTN);
    }
}
