package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PreCartPage extends BasePage{
    public PreCartPage(WebDriver driver) {
        super(driver);
    }

    public static By MONTHS_WARRANTY = By.xpath("//input[@value = '4041279']");
    public static By IR_PARA_CARRINHO_BTN = By.xpath("//button[contains( text(), 'IR PARA O CARRINHO')]");

    public void selectWarranty() {
        click(MONTHS_WARRANTY);
    }

    public void goToCart() {
        click(IR_PARA_CARRINHO_BTN);
    }
}
