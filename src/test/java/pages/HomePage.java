package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// classe herda da basepage para que seja possivel utilizar o driver e realizar as acoes necessarias
public class HomePage extends BasePage{
    public HomePage(WebDriver driver) {
        super(driver);
    }

    // tipo de mapeamento com os locators. podendo realizar as buscas de varias formas. id, xpath, class, link, csselector(...)

    public static By SEARCH_INPUT = By.xpath("//input[@id = 'input-busca']");
    public static By SEARCH_BTN = By.xpath("//button[@type = 'submit']");
    public static By DIV_NEXT = By.id("__next");
    public static By ACCEPT_COOKIES = By.id("onetrust-accept-btn-handler");

    public void fill_product_home(String product) {
        refreshPage();
        click(ACCEPT_COOKIES);
        find(DIV_NEXT);
        send_keys(SEARCH_INPUT, product);
        click(SEARCH_BTN);
    }
}
