package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

// classe responsavel de pegar a extensao do driver e as principais acoes para comunicacao com os elementos da pagina
public class BasePage {
    // o driver esta definido como protected para que seja possivel utiliza-la em subclasses.
    // por exemplo todas as classe de Page herdarao da BasePage e com isso terao a extensao do driver e todos os metodos
    protected WebDriver driver;

    //responsavel por gerar logs necessarios para o acompanhamento da execucao.
    private static final Logger logger = Logger.getLogger(BasePage.class.getName());

    // aqui é o construtor, metodo que sera executado quando um objeto da classe é criado. ou seja, sera incializado
    // o atributo que nesse caso é o driver.
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }


    // funcao responsavel de espera explicita. diferentemente da implicita que é utilizada uma vez. a explicita realiza
    // esperas customizadas de acordo com a necessidade do elemento. pode esperar elemento estar visivel, clicavel, entre outros...
    public WebDriverWait wait_for() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        return wait;
    }

    // metodo responsavel de encontrar o elemento
    public WebElement find(By locator) {
        WebElement element = wait_for().until(ExpectedConditions.elementToBeClickable(locator));
        return element;
    }

    // metodo responsavel de obter o texto atraves do atributo do elemento.
    public String getTextByAttribute(By locator, String attribute) {
        logger.info("-- obtendo o texto do elemento " + locator + " atraves do atributo " + attribute);
        return find(locator).getAttribute(attribute);
    }

    // metodo responsavel de clicar no elemento
    public void click(By locator) {
        wait_for().until(ExpectedConditions.elementToBeClickable(locator)).click();
        logger.info("-- clicando no elemento " + locator);
    }

    // metodo responsavel de preencher os campos
    public void send_keys(By locator, String text) {
        wait_for().until(ExpectedConditions.elementToBeClickable(locator)).sendKeys(text);
        // perceba que em cada funcao tem o log para facilitar uma possivel analise da execucao.
        logger.info("-- escrevendo no elemento " + locator);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }

}
