package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage extends BasePage{
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public static By FIRST_PRODUCT = By.xpath("//div[contains(@class, 'productCard')]");

    public void chooseFirstProduct() {
        click(FIRST_PRODUCT);
    }

}
