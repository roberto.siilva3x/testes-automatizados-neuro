package utils;

import io.cucumber.java.Scenario;
import io.cucumber.plugin.event.PickleStepTestStep;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import support.Driver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Screenshot {
    private static String path;
    private static SimpleDateFormat timestampEvidencia;
    private static SimpleDateFormat timeStampPasta;
    public static Scenario scenario;

// classe responsavel para tirar o screenshot da tela fazendo o uploate no target dividido por nome do cenario e respectiva data
    public static void takeScreenShot(){
        String nomePrint = "Evidencia";
        Date date = new Date();
        File scrFile = (File)((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.FILE);
        try{
            FileUtils.copyFile(scrFile, new File(path + scenario.getName().replace(" ", "_") + ""  + "/" +
                    timeStampPasta.format(date) + "/" + nomePrint + "_" +
                    timestampEvidencia.format(date)+ ".png"));
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    static {
        path = "target" + File.separator + "evidencias"
                + File.separator + "screenshot" + File.separator;
        timestampEvidencia = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        timeStampPasta = new SimpleDateFormat("yyMMdd");
    }

    public void removeDirectoryEvidence() {
        File directory = new File(System.getProperty("user.dir") + "/src/test/resources/relatorios");
        try {
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
