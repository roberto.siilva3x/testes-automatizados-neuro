package support;

import constants.Path;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static constants.Path.*;
import static constants.browserstack.Config.ACCESS_KEY;
import static constants.browserstack.Config.URL_BROWSERSTACK;
import static constants.browserstack.Config.USERNAME;

public class Driver {

    public static WebDriver driver;
    //metodo responsavel por inicializar o driver com as configuracoes necessarias para que seja possivel se comunicar
    // com os elementos da pagina web. cada driver deve ser usado especifico para o seu resp sistema operacional
    public static WebDriver getDriver() {

        if (driver == null) {
            ChromeOptions options = new ChromeOptions();
            if (Path.nomePc.contains("Mac")) {
                System.out.println("==== LOADING macIOS DRIVER ====");
                System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_IOS);
            }
            else if (Path.nomePc.contains("home")) {
                System.out.println("==== LOADING LINUX DRIVER ====");
                System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_LINUX);
                // executa a automacao e headless, ou seja a automacao é executada em background
                options.addArguments("--headless");
            }
            else {
                System.out.println("==== LOADING WINDOWS DRIVER ====");
                System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_WINDOWS);
            }

            // desativa as notificacoes do navegador
            options.addArguments("--disable-notifications");
            // permite que o Chrome faça solicitações CORS para qualquer origem
            options.addArguments("--remote-allow-origins=*");

            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            // implicity wait é uma forma de realizar uma espera, utilizado apenas uma vez no codigo. nesse exemplo,
            // aguarda a pagina estar totalmente carregada, dessa maneira ela funcionara para o projeto como um todo.
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        }
        return driver;
    }

    // metodo responsavel por inicializar as config necessarias para executar automacao no browserstack
    // é possivel simular diversos ambiente no browserstack, versoes e tipos de navegadores diferentes, sistemas operacionais, etc...
    public static WebDriver createBrowserStack() throws MalformedURLException {
        if (driver == null) {
            System.out.println("==== BROWSERSTACK LOADING ====");
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "110.0");
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("name", "JUnit Test");

            driver = new RemoteWebDriver(new URL("https://" + USERNAME + ":" + ACCESS_KEY +
                    URL_BROWSERSTACK), caps);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        }
        return driver;
    }


    // metodo responsavel de finalizar a sessao
    public static void endSession() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

}
