package steps;

import io.cucumber.java.es.Dado;
import pages.HomePage;

import static support.Driver.driver;

public class HomeStep {

    HomePage homePage = new HomePage(driver);

    @Dado("que realizo a pesquisa pelo produto {string}")
    public void searchProduct(String product) throws InterruptedException {
        homePage.fill_product_home(product);
    }
}
