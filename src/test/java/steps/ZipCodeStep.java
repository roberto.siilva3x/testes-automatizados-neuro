package steps;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pages.ZipCodePage;

import static support.Driver.driver;

public class ZipCodeStep {

    ZipCodePage zipCodePage = new ZipCodePage(driver);

    @Quando("preencho o cep com {string}")
    public void fillZipCode(String zipcode) throws InterruptedException {
        zipCodePage.fillZipCode(zipcode);
    }

    @Entao("imprimo o valor dos fretes")
    public void printPriceFrete() throws InterruptedException {
        zipCodePage.getAndPrintZipCode();
    }

    @Quando("adiciono o item no pre carrinho")
    public void addItemToPreCart() throws InterruptedException {
        zipCodePage.clickOnComprarBtn();
    }


}
