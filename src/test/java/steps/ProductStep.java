package steps;

import io.cucumber.java.es.Dado;
import pages.ProductPage;

import static support.Driver.driver;

public class ProductStep {

    ProductPage productPage = new ProductPage(driver);

    @Dado("adiciono o primeiro item para a compra")
    public void selectFirstItem() throws InterruptedException {
        productPage.chooseFirstProduct();
    }
}
