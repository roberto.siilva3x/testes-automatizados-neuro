package steps;

import io.cucumber.java.pt.Quando;
import pages.PreCartPage;

import static support.Driver.driver;

public class PreCartStep {

    PreCartPage preCartPage = new PreCartPage(driver);

    @Quando("seleciono 12 meses de garantia")
    public void addWarranty() throws InterruptedException {
        preCartPage.selectWarranty();
    }

    @Quando("adiciono o item no carrinho")
    public void addToCart() throws InterruptedException {
        preCartPage.goToCart();
    }
}
