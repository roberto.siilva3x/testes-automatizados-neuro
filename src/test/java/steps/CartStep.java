package steps;

import io.cucumber.java.pt.Entao;
import pages.CartPage;

import static support.Driver.driver;

public class CartStep {

    CartPage cartPage = new CartPage(driver);

    @Entao("valido que o produto esta no carrinho")
    public void productCartValidate() throws InterruptedException {
        cartPage.productCartValidate();
    }
}
