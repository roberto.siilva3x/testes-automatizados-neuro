package steps;

import io.cucumber.java.*;
import support.Driver;
import utils.Screenshot;

import java.net.MalformedURLException;

import static constants.browserstack.Config.ENVIRONMENT;
import static constants.Url.MAIN_URL;


// classe responsavel para preparar o ambiente para que executar os testes, bem como voltar para o estado inicial apos execucao.
public class ServiceHooks {
// essa funcao é executada sempre antes de cada cenario, nesse exemplo ira abrir o navegador
    @Before
    public void beforeScenario(Scenario scenario) throws InterruptedException, MalformedURLException {
        Screenshot.scenario = scenario;
        if (ENVIRONMENT.equals("browserstack")) {
            Driver.createBrowserStack().get(MAIN_URL);
        }
        else {
            Driver.getDriver().get(MAIN_URL);
        }
    }

    // essa anotacao serve para executar a funcao toda vez antes de cada passo. nesse exemplo sera tirado um print
    // da tela para facilitar uma possivel analise.
    @BeforeStep
    public void beforeStep() throws InterruptedException {
        Thread.sleep(1000);
        Screenshot.takeScreenShot();
    }

    @AfterStep
    public void afterStep() throws InterruptedException {
        Thread.sleep(1000);
        Screenshot.takeScreenShot();
    }

    @After
    public void tearDown() throws InterruptedException {
        Driver.endSession();
    }
}
