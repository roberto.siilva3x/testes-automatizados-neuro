#language: pt
Funcionalidade: Carrinho de compras

  ## as tags servem para identificar caracteristicas do cenario. com isso podera customizar as execucoes da automacao
  ## de acordo com a necessidade.
  @smoke @carrinho
  Cenario: Verifique que o produto é exibido no carrinho
    Dado que realizo a pesquisa pelo produto "notebook"
    E adiciono o primeiro item para a compra
    Quando preencho o cep com "54440350"
    Entao imprimo o valor dos fretes
    Quando adiciono o item no pre carrinho
    E seleciono 12 meses de garantia
    E adiciono o item no carrinho
    Entao valido que o produto esta no carrinho