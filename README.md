# testes-automatizados-neuro

# Tecnologias
* Este projeto foi desenvolvido em java, junit framework responsavel pelas validacoes, selenium para comunicacao da pagina web,
e cucumber para descricao dos cenarios a serem validado em linguagem natural.

##  DOWNLOADS E INSTALACAO MAC
* Java 8+ JDK deve estar instalado
    * https://www.java.com/pt_BR/download/
* Maven deve estar instalado e configurado no path da aplicação
    * Realize download https://maven.apache.org/download.cgi e descompacte
    * No seu terminal, insira o comando: defaults write com.apple.finder AppleShowAllFiles -bool true

## Configurando variaveis de ambiente no MAC
* Caso nao exista o arquivo .profile crie
    * touch ~/.profile
    * nano ~/.profile
    * adicione as variáveis de ambiente: JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk/Contents/Home
      export JAVA_HOME
      J6=$JAVA_HOME/bin
      M3_HOME=/Users/robertofilho/downloads/apache-maven-3.9.1
      export M3_HOME
      M3=$M3_HOME/bin
      PATH=$M3:$PATH:$J6
      export PATH
    * Agora carregue o arquivo com comando: source ~/.profile
    * Para salvar clique: Ctlr+X depois Y e por fim ENTER
    * source ~/.profile
    * verifique se o maven foi instalado com comando mvn - version
    * Caso nao seja possivel visualizar o maven, instale via terminal com:
        * brew install maven
    * Ao final volte as configuracoes com:
        * defaults write com.apple.finder AppleShowAllFiles -bool false

## ARQUITETURA
* Design pattern de Page Object que consiste em criar camadas de abstracao, tornando o codigo mais organizado e consequentemente facilita a manutencao.
* A classe BasePage é a base para obter reutilizacao do driver e tambem os metodos de acoes em outras classes que a herda. Por exemplo, outras pages.

## Regras de desenvolvimento
* Afim de obter a melhor experiencia, escalabilidade e melhor manutencao no codigo, siga os passos para que seja implementados novos cenarios de automacao:
    * Crie o arquivo com extensao.feature com o nome da funcionalidade que deseja validar em src/test/resources/features
    * Crie o arquivo com os steps com o nome da pagina que deseja automatizar em src/test/java/steps
        * Obs: Assegure que seja implementado o step com apenas o que ele deve realmente fazer. nada mais nada menos.
    * Para inserir regras e logicas de metodos, utilize o arquivo src/test/java/pages/BasePage
        * Obs: Assegure que a logica que voce deseja nao esta realmente implementada nessa classe
    * Para mapear os elementos de uma nova pagina, crie um novo arquivo em src/test/java/pages/<nomePaginaPage>
    * Para mapear os elementos siga o mesmo padrao de locators com By(caso precise manipular de outra forma, alinhar com a lideranca)
        * Abaixo crie os metodos das acoes que deseja fazer nessa pagina
    * Para utilizar os metodos da pagina nos steps, instancie a pagina deseja. por ex: HomePage homePage new HomePage(), e assim podera usar a funcao por ex: homePage.fill_product_home() no step desejado;
    * Caso seus testes tenham mais de uma URL, centralize no arquivo src/test/java/constants/Url
    * Para manipular diferentes versoes de drivers ou navegadores, adicionar em src/test/java/common/drivers
    * Para mensagens fixas seja de sucesso ou erro, adicione src/test/java/constants/
    * O arquivo ServiceHooks tem o intuito de executar acoes antes e depois dos cenarios. Tenha cautela caso necessite inserir algo para nao impactar outros cenarios.
* Obs: Caso enxergue melhorias possiveis na estrutura do projeto, conversar com a lideranca para entender a melhor forma para ajustar.

## Como executar o projeto
* execute no terminal na raiz do projeto:
    * mvn test -Dtest=TestRunner cluecumber-report:reporting
* Ou voce pode executar direto no arquivo TestRunner
* Obs: Verifique se a versao do seu chrome esta compativel com o driver do projeto.
* Caso queira executar apontando para o browserstack é so alterar o env para "browserstack" no arquivo constants/browserstack/config
* Nota: Para o site da kabum, nao esta sendo possivel executar em ambiente controlado tanto no browserstack quanto em imagem docker. estou realizando analise para identificar a causa.
* Optei por adicionar opcoes de execucao com browserstack por que é uma otima plataforma de device farm. com ele é possivel verificar comportamentos:
1. Simular diferentes sistemas operacionais
2. Diferentes tipos de navegadores
3. Diferentes versoes de navegadores


## ANALISANDO RELATORIO
* É gerado evidencias com screenshot da tela de cada step para o respectivo cenario no diretorio /target/evidencias
* É gerado tambem o relatorio da execucao de todos os testes que passaram e falharam para levantamento de metricas
* Caminho: target/formated-report/index.html
  * Obs: esse relatorio é gerado atraves do plugin cluecumber-report-plugin
* Com essas informacoes fica mais facil analisar e compreender os passos que foram executados combinando a execucao dos logs, evidencias de print da tela, e o resultado final no relatorio.


* espero que gostem, obrigado!
